import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/LoginForm.vue'
import About from './views/About.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        breadcrumb: [
            { name: "Area 1", link: 'area1'},
            { name: "Area 2"}
        ]
      },
    },
    {
      path: '/about',
      name: 'about',
      component: About
    }
  ]
})
