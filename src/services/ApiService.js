import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";


const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = process.env.VUE_APP_API_URL;
    },
    setHeader() {
        console.warn($cookies.get('access_token'));

        if( $cookies.get('access_token') ) {
            let config = {
                    'Authorization': 'Bearer ' + $cookies.get('access_token')
            };
            Vue.axios.defaults.headers.common = config;
        }
    },
    query(resource, params) {
        return Vue.axios.get(resource, params)
    },

    get(resource) {
    //    console.info(localStorage.access_token);
        return Vue.axios.get( resource )
    },

    post(resource, params) {
        return Vue.axios.post(resource, params);
    },

    update(resource, params) {
        return Vue.axios.put(resource, params);
    },

    put(resource, params) {
        return Vue.axios.put(resource, params);
    },

    delete(resource) {
        return Vue.axios.delete(resource);
    }
};

export default ApiService;

export const LoginService = {
    get() {
        return ApiService.get();
    },
    auth( credentials ) {
        alert(credentials);
        /*
        return ApiService.post('api/oauth/token',{
            "client_id": process.env.VUE_APP_API_CLIENT_ID,
            "client_secret": process.env.VUE_APP_API_CLIENT_SECRET,
            "username": credentials.username,
            "password": credentials.password,
            "grant_type": "password"
        });
        */
    },
};


/*
export const TagsService = {
    get() {
        return ApiService.get("tags");
    }
};
export const ArticlesService = {
    query(type, params) {
        return ApiService.query("articles" + (type === "feed" ? "/feed" : ""), {
            params: params
        });
    },
    get(slug) {
        return ApiService.get("articles", slug);
    },
    create(params) {
        return ApiService.post("articles", { article: params });
    },
    update(slug, params) {
        return ApiService.update("articles", slug, { article: params });
    },
    destroy(slug) {
        return ApiService.delete(`articles/${slug}`);
    }
};

export const CommentsService = {
    get(slug) {
        if (typeof slug !== "string") {
            throw new Error(
                "[RWV] CommentsService.get() article slug required to fetch comments"
            );
        }
        return ApiService.get("articles", `${slug}/comments`);
    },

    post(slug, payload) {
        return ApiService.post(`articles/${slug}/comments`, {
            comment: { body: payload }
        });
    },

    destroy(slug, commentId) {
        return ApiService.delete(`articles/${slug}/comments/${commentId}`);
    }
};

export const FavoriteService = {
    add(slug) {
        return ApiService.post(`articles/${slug}/favorite`);
    },
    remove(slug) {
        return ApiService.delete(`articles/${slug}/favorite`);
    }
};
*/
