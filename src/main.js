import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import store from './store';

import VueCookies from 'vue-cookies'
Vue.use(VueCookies);
VueCookies.config('7d');
VueCookies.set('theme','default');

//import VueResource from 'vue-resource';
//Vue.use(VueResource);

// Api Service
import ApiService from './services/ApiService';
ApiService.init();

// Template Assets
import 'bootstrap';
import './assets/template/css/style.css';

router.beforeEach((to, from, next) => {
  console.warn( $cookies.get('access_token'));
  //if( !$cookies.get('access_token')){
   //   this.$router.push('login');
 // }
  return next(true);
});


// console.log(Vue.axios.defaults);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
