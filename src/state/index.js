

const getters = {
    getUser(state) {
        return state.user;
    }
};

const store = new Vuex.Store({
    state: {
        user: {},
        logged: false
    },
    mutations: {
        userIntroApp ( state, userData ) {
           state.user = userData;
        },
    },
    getters,
    actions: {
        userIntroApp ( data ) {
            //commit('userIntroApp', { todo, done: !todo.done })
            context.commit('userIntroApp' , data)
        },
    },
})
