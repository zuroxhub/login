
// State - Estado do usuário
const state = {
    errors: null,
    user: {},
    zurox: 'Oie Zub',
    isAuthenticated: localStorage.isAuthenticated
};

// Getters
const getters = {
    currentUser(state) {
        return state.user;
    }
};

// Actions
const actions = {

};

// Mutations - Mudanças de estado
export const mutations = {
    setUser(state, user) {
        state.user = user;
    }
};


export default {
    state,
    getters,
    actions,
    mutations
};
